#
# 用来描述区间节点的类
#
class Node
  attr_accessor :l_child # 区间最小值
  attr_accessor :r_child # 区间最大值

  #
  # 实例化区间对象的方法
  #
  # @param [Integer] l_child 区间最小值
  # @param [Integer] r_child 区间最大值
  #
  #
  def initialize(l_child, r_child)
    self.l_child = l_child
    self.r_child = r_child
  end

  #
  # 输出区间的字符串表示
  #
  # @return [String] 用字符串表示的区间
  #
  # 
  def print
    "[#{l_child}, #{r_child})"
  end
end

class RangeList
  attr_accessor :range_list # 保存已经添加的区间

  #
  # 初始化RangeList对象
  #
  def initialize
    self.range_list = []
  end

  #
  # 添加区间
  #
  # @param [Array] range 范围数组
  #
  #
  def add(range)
    new_node = Node.new(range[0], range[1])
    temp_range_list = []
    range_list.each do |node|
      if node.r_child < new_node.l_child || node.l_child > new_node.r_child
        # 两个不同的区间
        temp_range_list << node
      else
        # 相交区间
        new_node.l_child = [node.l_child, new_node.l_child].min
        new_node.r_child = [node.r_child, new_node.r_child].max
      end
    end
    temp_range_list << new_node
    # 排序
    self.range_list = temp_range_list.sort{|a, b| a.l_child <=> b.l_child}
  end

  #
  # 删除区间
  #
  # @param [Array] range 需要删除的区间范围
  #
  #
  def remove(range)
    new_node = Node.new(range[0], range[1])
    temp_range_list = []
    self.range_list.each do |node|
      if node.l_child <= new_node.r_child && node.r_child >= new_node.l_child
        # 相交区间
        # 1、部分相交
        if node.l_child < new_node.l_child
          temp_range_list << Node.new(node.l_child, new_node.l_child)
        end
        if node.r_child > new_node.r_child
          temp_range_list << Node.new(new_node.r_child, node.r_child)
        end
      else
        temp_range_list << node
      end
    end
    # 排序
    self.range_list = temp_range_list.sort{|a, b| a.l_child <=> b.l_child}
  end

  #
  # 打印出所有的区间范围
  #
  # @return [String] 字符串表现的区间范围
  #
  # 
  def print
    self.range_list.map(&:print).join(" ")
  end
end
