require 'minitest/autorun'
require './range_list.rb'

describe RangeList do
  it '输入[1, 5]' do
    rl = RangeList.new
    rl.add([1, 5])
    rl.print.must_equal '[1, 5)'
    rl.add([10, 20])
    rl.print.must_equal '[1, 5) [10, 20)'
    rl.add([20, 20])
    rl.print.must_equal '[1, 5) [10, 20)'
    rl.add([20, 21])
    rl.print.must_equal '[1, 5) [10, 21)'
    rl.add([2, 4])
    rl.print.must_equal '[1, 5) [10, 21)'
    rl.add([3, 8])
    rl.print.must_equal '[1, 8) [10, 21)'
    rl.remove([10, 10])
    rl.print.must_equal '[1, 8) [10, 21)'
    rl.remove([10, 11])
    rl.print.must_equal '[1, 8) [11, 21)'
    rl.remove([15, 17])
    rl.print.must_equal '[1, 8) [11, 15) [17, 21)'
    rl.remove([3, 19])
    rl.print.must_equal '[1, 3) [19, 21)'
  end
end
